function countPositivesSumNegatives(input) {
    let result = [];
    let countPositive = 0;
    let sumNegative = 0;

    if(input && input.length) {
      for(let i = 0; i < input.length; i++) {
        if(input[i] > 0) {
            countPositive += 1;
        } else {
            sumNegative += input[i];
        }
      }
      result.push(countPositive);
      result.push(sumNegative);
    }
    return result;
}

console.log(countPositivesSumNegatives([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]));